package com.example.springiocdemo;

public interface Coach {

    public String getDailyWorkout();
}
