package com.example.ioc;

import com.example.springiocdemo.BaseballCoach;
import com.example.springiocdemo.Coach;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IocApplication {

    public static void main(String[] args) {
        SpringApplication.run(IocApplication.class, args);

        //create the object
        Coach theCoach = new BaseballCoach();

        //use the object
        System.out.println(theCoach.getDailyWorkout());
    }

}
